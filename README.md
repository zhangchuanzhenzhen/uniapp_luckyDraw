# uniapp九宫格_转盘抽奖

#### 体验
  http://rcchome.gitee.io/uniapp_luckydraw/#/

#### 介绍
  uniapp项目,九宫格/转盘抽奖,微信小程序,H5,可设置中奖概率


#### 安装教程

1. git clone # uniapp九宫格_转盘抽奖

#### 使用说明

1.  使用HBuider打开
2.  运行到微信开发者工具或浏览器
3.  输入口令为当前 小时+分钟, 4位数, 例如: 1212
